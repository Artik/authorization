<?php

class Auth
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function isAuth()
    {
        if (isset($_SESSION["auth"])) {
            return $_SESSION["auth"];
        } else {
            return false;
        }
    }

    public function auth($email, $password)
    {
        $query = "SELECT * FROM `users` WHERE `email`='$email'";
        $res = mysqli_query($this->db, $query);

        $data = [
            'status' => 1,
            'errors' => [],
            'user' => []
        ];

        $result = array();
        while ($row = mysqli_fetch_assoc($res)) {
            $result[] = $row;
        }

        if (!count($result)) {
            $data['status'] = 0;
            $data['errors'][] = 'User with the specified email address does not exist';
            return $data;
        }

        $user = $result[0];

        if (md5($password) != $user['password']) {
            $data['status'] = 0;
            $data['errors'][] = 'Incorrect password';
            return $data;
        }

        $_SESSION["auth"] = $email;
        $data['user'] = $user;

        return $data;
    }

    public function logout()
    {
        try {
            session_destroy();
            return $data = [
                'status' => 1,
                'errors' => []
            ];
        } catch (Exception $e) {
            return $data = [
                'status' => 0,
                'errors' => [$e]
            ];
        }
    }
}