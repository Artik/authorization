<?php

class User
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function create($email, $password, $name, $age)
    {
        $data = [
            'status' => 1,
            'errors' => []
        ];

        if (!$this->checkEmail($email)) {
            $data['status'] = 0;
            $data['errors'][] = 'Bad email';
            return $data;
        }

        $user0 = $this->get('email', $email);
        if ($user0['status'] == 1) {
            $data['status'] = 0;
            $data['errors'][] = 'User with the specified email address already exists';
            return $data;
        }

        if (strlen($password) < 6) {
            $data['status'] = 0;
            $data['errors'][] = 'Password must contain at least 6 characters';
            return $data;
        }

        $password = md5($password);
        $age = intval($age);

        $query = "INSERT INTO `auth`.`users` ( `email`, `password`, `name`, `age`) VALUES ( '$email', '$password', '$name', '$age');";
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $data['status'] = 0;
            $data['errors'][] = 'DB Error';
        }
        return $data;
    }

    public function createByHash($email, $hash)
    {
        $data = [
            'status' => 1,
            'errors' => []
        ];

        if (!$this->checkEmail($email)) {
            $data['status'] = 0;
            $data['errors'][] = 'Bad email';
            return $data;
        }

        $user0 = $this->get('email', $email);
        if ($user0['status'] == 1) {
            $data['status'] = 0;
            $data['errors'][] = 'User with the specified email address already exists';
            return $data;
        }

        $age = 0;
        $name = '';

        $query = "INSERT INTO `auth`.`users` ( `email`, `password`, `name`, `age`) VALUES ( '$email', '$hash', '$name', '$age');";
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $data['status'] = 0;
            $data['errors'][] = 'DB Error';
        }
        return $data;
    }

    public function delete($id)
    {
        $data = [
            'status' => 1,
            'errors' => []
        ];

        if (!$id) {
            $data['status'] = 0;
            $data['errors'][] = 'ID is required';
            return $data;
        }

        $query = "DELETE FROM `auth`.`users` WHERE `users`.`id` = $id";
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $data['status'] = 0;
            $data['errors'][] = 'DB Error';
        }
        return $data;
    }

    public function deleteByAPI($email, $password)
    {
        $data = [
            'status' => 1,
            'errors' => []
        ];
        $query = "SELECT * FROM `users` WHERE `email`='" . $email . "'";
        $res = mysqli_query($this->db, $query);
        $result = array();
        while ($row = mysqli_fetch_assoc($res)) {
            $result[] = $row;
        }
        if (!count($result)) {
            $data['status'] = 0;
            $data['errors'][] = 'User with the specified email address does not exist';
            return $data;
        }
        $user = $result[0];

        if (md5($password) != $user['password']) {
            $data['status'] = 0;
            $data['errors'][] = 'Incorrect password';
            return $data;
        }

        $query = "DELETE FROM `auth`.`users` WHERE `users`.`id` = '" . $user['id'] . "'";
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $data['status'] = 0;
            $data['errors'][] = 'DB Error';
        }
        return $data;
    }

    public function get($field, $value)
    {
        $data = [
            'status' => 1,
            'errors' => []
        ];
        $query = "SELECT id, email, name, age FROM `users` WHERE `$field`='$value'";

        $res = mysqli_query($this->db, $query);
        $result = array();
        while ($row = mysqli_fetch_assoc($res)) {
            $result[] = $row;
        }

        if (!count($result)) {
            $data['status'] = 0;
            $data['errors'][] = "User with the specified $field does not exist";
            return $data;
        }

        $data['user'] = $result[0];

        return $data;
    }

    private function checkEmail($emailaddress)
    {
        $pattern = '/([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])/';
        return preg_match($pattern, $emailaddress);
    }
}