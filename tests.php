<?php
include_once 'db.php';
include_once 'auth.php';
include_once 'user.php';

session_start();

$tests = [];
function check($test_name, $checkfn)
{
    try {
        $value = $checkfn();
        if ($value) {
            return ['name' => $test_name, 'status' => true];
        } else {
            return ['name' => $test_name, 'status' => false];
        }
    } catch (Exception $e) {
        echo $e;
        return ['name' => $test_name, 'status' => false];
    }
}

function deleteUser($email)
{
    $db = new db();
    $user = new User($db->connection());
    $user0 = $user->get('email', $email);

    if ($user0['status'] == 1) {
        $user->delete($user0['user']['id']);
    }
}

$tests[] = check('test ' . (count($tests) + 1) . ' - create user (ololo@lol.ol)', function () {
    $db = new db();
    $user = new User($db->connection());
    $data = $user->create('ololo@lol.ol', 'ololol', 'Ololosha', 12);
    return ($data['status'] == 1);
});

$tests[] = check('test ' . (count($tests) + 1) . ' - read user (ololo@lol.ol)', function () {
    $db = new db();
    $auth = new Auth($db->connection());
    $data = $auth->auth('ololo@lol.ol', 'ololol');
    return ($data['status'] == 1);
});

$tests[] = check('test ' . (count($tests) + 1) . ' - log out', function () {
    $db = new db();
    $auth = new Auth($db->connection());
    $data = $auth->logout();
    return ($data['status'] == 1);
});

$tests[] = check('test ' . (count($tests) + 1) . ' - create user with the same email (ololo@lol.ol)', function () {
    $db = new db();
    $user = new User($db->connection());
    $data = $user->create('ololo@lol.ol', 'ololol', 'Ololosha', 42);
    // status 0 = error
    return ($data['status'] == 0);
});

$tests[] = check('test ' . (count($tests) + 1) . ' - delete user (ololo@lol.ol)', function () {
    $data = [];
    $db = new db();
    $user = new User($db->connection());
    $user0 = $user->get('email', 'ololo@lol.ol');
    if ($user0['status'] == 1) {
        $st = $user->delete($user0['user']['id']);
        if ($st['status'] == 1) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
    } else {
        $data['status'] = 0;
    }
    return ($data['status'] == 1);
});

$tests[] = check('test ' . (count($tests) + 1) . ' - create user with bad email (o@l.o)', function () {
    $db = new db();
    $user = new User($db->connection());
    $data = $user->create('o@l.o', 'ololol', 'Ololosha', 42);
    return ($data['status'] == 0);
});

$tests[] = check('test ' . (count($tests) + 1) . ' - create user without password', function () {
    $db = new db();
    $user = new User($db->connection());
    $data = $user->create('userq@test.com', '', 'User', 23);
    return ($data['status'] == 0);
});

deleteUser('ololo@lol.ol');
deleteUser('o@l.o');
deleteUser('userq@test.com');
?>

<?php foreach ($tests as $d): ?>
	<p>
        <?= $d['name'] ?> —
		<span style="color:<?= $d['status'] ? 'green' : 'red' ?>">
			<?= $d['status'] ? 'success' : 'failed' ?>
		</span>
	</p>
<?php endforeach; ?>
