angular.module('authApp', ['ui-notification', 'ngCsvImport'])
	.controller('authController', function($scope, $http, Notification) {
		$scope.showSingInForm = false;
		$scope.authPage = false;
		$scope.userPage = false;
		$scope.modalOpen = false;

		function clear() {
			$scope.name = '';
			$scope.age = 18;
			$scope.email = '';
			$scope.password = '';
			$scope.dpass = '';

			$scope.user = {
				email: '',
				name: '',
				age: 0
			};
		}

		clear();

		function notify(errors) {
			errors.forEach(function(item) {
				Notification.error(item);
			})
		}

		function auth() {
			$http.post('/api.php', {
				action: 'auth'
			}).then(function(resp) {
				console.log(resp.data);
				if (resp.data.auth) {
					$scope.authPage = false;
					$scope.userPage = true;
					$scope.user.email = resp.data.user.email;
					$scope.user.name = resp.data.user.name;
					$scope.user.age = resp.data.user.age;
				} else {
					$scope.authPage = true;
					$scope.userPage = false;
					clear();
				}
			}, function(resp) {
				console.warn(resp);
				notify(resp.data.errors);
			});
		}

		$scope.changeForm = function() {
			$scope.showSingInForm = !$scope.showSingInForm;
		};

		$scope.login = function() {
			$http.post('/api.php', {
				action: 'login',
				email: $scope.email,
				password: $scope.password
			}).then(function(resp) {
				console.log(resp.data);
				if (resp.data.status) {
					$scope.authPage = false;
					$scope.userPage = true;
					$scope.user.email = resp.data.user.email;
					$scope.user.name = resp.data.user.name;
					$scope.user.age = resp.data.user.age;
				} else {
					notify(resp.data.errors);
				}
			}, function(resp) {
				console.warn(resp);
				notify(resp.data.errors);
			});
		};

		$scope.logout = function() {
			$scope.user.email = '';
			$scope.user.name = '';
			$scope.user.age = 18;
			$http.post('/api.php', {
				action: 'logout'
			}).then(function(resp) {
				console.log(resp.data);
				if (resp.data.status) {
					$scope.authPage = true;
					$scope.userPage = false;
				} else {
					notify(resp.data.errors);
				}
			}, function(resp) {
				console.warn(resp);
				notify(resp.data.errors);
			});
		};

		$scope.singup = function() {
			$http.post('/api.php', {
				action: 'singup',
				email: $scope.email,
				password: $scope.password,
				name: $scope.name,
				age: $scope.age
			}).then(function(resp) {
				console.log(resp.data);
				if (resp.data.status) {
					$scope.authPage = false;
					$scope.userPage = true;
					$scope.showSingInForm = false;
					$scope.login();
				} else {
					notify(resp.data.errors);
				}
			}, function(resp) {
				console.warn(resp.data);
				notify(resp.data.errors);
			});
		};

		$scope.delete = function() {
			$http.post('/api.php', {
				action: 'delete',
				password: $scope.dpass
			}).then(function(resp) {
				console.log(resp.data);
				if (resp.data.status) {
					$scope.showSingInForm = false;
					$scope.authPage = true;
					$scope.userPage = false;
					$scope.modalOpen = false;
					clear();
				} else {
					notify(resp.data.errors);
				}
			}, function(resp) {
				console.warn(resp);
				notify(resp.data.errors);
			});
		};

		auth();

		$scope.csv = {
			content: null,
			header: true,
			headerVisible: true,
			separator: ';',
			separatorVisible: true,
			result: null,
			encoding: 'ISO-8859-1',
			encodingVisible: false,
			uploadButtonLabel: "upload a csv file"
		};

		$scope.$watch('csv.result', function(json) {
			// console.log(json);

			if (Array.isArray(json)) {
				console.log(json)
				var out = json.filter(function(item) {
					return (item.hasOwnProperty('user') && item.user.length
					&& item.hasOwnProperty('password_hash') && item.password_hash.length);
				});

				console.log(out);

				if (out.length) {
					$http.post('/api.php', {
						action: 'uploadDB',
						db: out
					}).then(function(resp) {
						console.log(resp.data);
						if (resp.data.status) {
							Notification.success('Uploading complete');
						} else {
							notify(resp.data.errors);
						}
					}, function(resp) {
						console.warn(resp);
						notify(resp.data.errors);
					});
				} else {
					Notification.warning('The file is damaged');
				}
			}
		});
	});