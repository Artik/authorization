<?php

class db
{
    private $dblocation = "localhost";
    private $dbname = "auth";
    private $dbuser = "root";
    private $dbpasswd = "";
    private $connection;

    function __construct()
    {
        $dbConnection = mysqli_connect($this->dblocation, $this->dbuser, $this->dbpasswd, $this->dbname);
        if (!$dbConnection) {
            die('DB Error');
        }
        mysqli_query($dbConnection, 'set names utf8');
        $this->connection = $dbConnection;
        $this->create();
    }

    public function connection()
    {
        return $this->connection;
    }

    private function create()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS `users` (
              `id` int(11) NOT NULL,
              `email` text NOT NULL,
              `password` text NOT NULL,
              `name` text NOT NULL,
              `age` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        mysqli_query($this->connection, $query);

        $query = "ALTER TABLE `users` ADD PRIMARY KEY (`id`);";
        mysqli_query($this->connection, $query);

        $query = "ALTER TABLE `users` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        mysqli_query($this->connection, $query);
    }
}


