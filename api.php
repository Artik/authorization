<?php
include_once 'db.php';
include_once 'user.php';
include_once 'auth.php';

header('Content-Type: application/json');
session_start();

// TODO: fix server?
$post = json_decode(file_get_contents("php://input"), true);

$action = $post['action'];
$data = [
    'errors' => []
];

if (empty($action)) {
    array_push($data ['errors'], 'action is empty');
    echo json_encode($data);
    return;
}

$db = new db();
$auth = new Auth($db->connection());
$user = new User($db->connection());

if ($action == 'auth') {
    $a = $auth->isAuth();
    $data['auth'] = $a;
    if ($a) {
        $data['auth'] = true;
        $data['user'] = $user->get('email', $a)['user'];
    }
    echo json_encode($data);
    return;
}

if ($action == 'login') {
    $email = $post['email']; //$_POST['email'];
    $passw = $post['password']; //$_POST['password'];

    $data = $auth->auth($email, $passw);

    echo json_encode($data);
    return;
}

if ($action == 'logout') {
    $auth->logout();
    $data['status'] = 1;
    echo json_encode($data);
    return;
}

if ($action == 'singup') {
    $email = $post['email']; //$_POST['email'];
    $passw = $post['password']; //$_POST['password'];
    $name = $post['name'];
    $age = $post['age'];
    $data = $user->create($email, $passw, $name, $age);

    echo json_encode($data);
    return;
}

if ($action == 'delete') {
    $passw = $post['password'];

    $data = $user->deleteByAPI($_SESSION["auth"], $passw);
    $auth->logout();

    echo json_encode($data);
    return;
}

if ($action == 'uploadDB') {
    $db = $post['db'];

    foreach ($db as $item) {
        $data = array_merge($data, $user->createByHash($item['user'], $item['password_hash']));
    }

    echo json_encode($data);
    return;
}

$data['errors'][] = 'Bad request';
echo json_encode($data);
return;
